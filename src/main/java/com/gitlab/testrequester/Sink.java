package com.gitlab.testrequester;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Sink {
    String method;

    String uri;

    Object body;

    @NonNull
    Map<String, List<String>> headers = Collections.emptyMap();

    @NonNull
    Map<String, List<String>> queryParams = Collections.emptyMap();
}
