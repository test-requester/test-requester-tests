package com.gitlab.testrequester;

import lombok.Value;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Base64;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

@SuppressWarnings({"java:S1192", "java:S100", "DuplicatedCode"})
public abstract class TestRequesterAbstractTest {

    protected final Sink sink = new Sink();

    protected abstract TestRequester requester();

    @Test
    void test_head() {
        // ARRANGE
        // ACT
        String body = requester().head("/echo").execute()
                .assertStatusOk()
                .getBody();

        // ASSERT
        assertThat(body).as("body").isEmpty(); // HEAD cannot receive a body as response
        assertThat(sink.getMethod()).as("Method").isEqualTo("HEAD");
        assertThat(sink.getUri()).as("URI").isEqualTo("/echo");
        assertThat(sink.getBody()).as("Body ignored").isNull();
        assertThat(sink.getHeaders()).as("Headers").isEmpty();
        assertThat(sink.getQueryParams()).as("Parameters").isEmpty();
    }

    @Test
    void test_options() {
        // ARRANGE
        // ACT
        String body = requester().options("/echo").execute()
                .assertStatusOk()
                .getBody();

        // ASSERT
        // Method body not executed for OPTIONS - no body, no sink args
        assertThat(body).as("body").isEmpty();
        assertThat(sink.getMethod()).as("Method").isNull();
        assertThat(sink.getUri()).as("URI").isNull();
        assertThat(sink.getBody()).as("Body ignored").isNull();
        assertThat(sink.getHeaders()).as("Headers").isEmpty();
        assertThat(sink.getQueryParams()).as("Parameters").isEmpty();
    }

    @Test
    void test_get() {
        // ARRANGE
        // ACT
        String body = requester().get("/echo").execute()
                .assertStatusOk()
                .getBody();

        // ASSERT
        assertThat(body).as("body").isEqualTo("null");
        assertThat(sink.getMethod()).as("Method").isEqualTo("GET");
        assertThat(sink.getUri()).as("URI").isEqualTo("/echo");
        assertThat(sink.getBody()).as("Body ignored").isNull();
        assertThat(sink.getHeaders()).as("Headers").isEmpty();
        assertThat(sink.getQueryParams()).as("Parameters").isEmpty();
    }

    @Test
    void test_delete() {
        // ACT
        String body = requester().delete("/echo").execute()
                .assertStatusOk()
                .getBody();

        // ASSERT
        assertThat(body).as("body").isEqualTo("null");
        assertThat(sink.getMethod()).as("Method").isEqualTo("DELETE");
        assertThat(sink.getUri()).as("URI").isEqualTo("/echo");
        assertThat(sink.getBody()).as("Body ignored").isNull();
        assertThat(sink.getHeaders()).as("Headers").isEmpty();
        assertThat(sink.getQueryParams()).as("Parameters").isEmpty();
    }

    @Test
    void test_post() {
        // ACT
        Dto dto = new Dto(12, "twelve");
        TestResponse response = requester().post("/echo")
                .withJsonBody(dto)
                .execute()
                .assertStatusOk();

        // ASSERT
        assertThat(response.getBody()).as("body json")
                .isEqualTo("{\"number\":12,\"text\":\"twelve\"}");
        assertThat(response.getJsonBody(Dto.class)).as("body parsed")
                .isEqualTo(dto);

        assertThat(sink.getMethod()).as("Method").isEqualTo("POST");
        assertThat(sink.getUri()).as("URI").isEqualTo("/echo");
        assertThat(sink.getBody()).as("Body").isEqualTo(dto);
        assertThat(sink.getHeaders()).containsEntry("Content-Type", Lists.of("application/json"));
        assertThat(sink.getQueryParams()).as("Parameters").isEmpty();
    }

    @Test
    void test_put() {
        // ACT
        Dto dto = new Dto(12, "twelve");
        TestResponse response = requester().put("/echo")
                .withJsonBody(dto)
                .execute()
                .assertStatusOk();

        // ASSERT
        assertThat(response.getBody()).as("body json")
                .isEqualTo("{\"number\":12,\"text\":\"twelve\"}");
        assertThat(response.getJsonBody(Dto.class)).as("body parsed")
                .isEqualTo(dto);

        assertThat(sink.getMethod()).as("Method").isEqualTo("PUT");
        assertThat(sink.getUri()).as("URI").isEqualTo("/echo");
        assertThat(sink.getBody()).as("Body").isEqualTo(dto);
        assertThat(sink.getHeaders()).containsEntry("Content-Type", Lists.of("application/json"));
        assertThat(sink.getQueryParams()).as("Parameters").isEmpty();
    }

    @Test
    void test_patch() {
        // ACT
        Dto dto = new Dto(12, "twelve");
        TestResponse response = requester().patch("/echo")
                .withJsonBody(dto)
                .execute()
                .assertStatusOk();

        // ASSERT
        assertThat(response.getBody()).as("body json")
                .isEqualTo("{\"number\":12,\"text\":\"twelve\"}");
        assertThat(response.getJsonBody(Dto.class)).as("body parsed")
                .isEqualTo(dto);

        assertThat(sink.getMethod()).as("Method").isEqualTo("PATCH");
        assertThat(sink.getUri()).as("URI").isEqualTo("/echo");
        assertThat(sink.getBody()).as("Body").isEqualTo(dto);
        assertThat(sink.getHeaders()).containsEntry("Content-Type", Lists.of("application/json"));
        assertThat(sink.getQueryParams()).as("Parameters").isEmpty();
    }

    @Test
    void test_withDefaultHeaders() {
        // ARRANGE
        Map<String, List<String>> defaultHeaders = new LinkedHashMap<>();
        defaultHeaders.put("Authorization", Lists.of("Basic dXNlcm5hbWU6cGFzc3dvcmQK"));
        defaultHeaders.put("RequestId", Lists.of("request:12"));
        defaultHeaders.put("MultiHeader", Lists.of("Override-me", "And-me"));
        TestRequester requester = requester().withDefaultHeaders(defaultHeaders);

        // ACT
        requester.get("/echo").execute().assertStatusOk();

        // ASSERT
        assertThat(sink.getHeaders()).containsEntry("Authorization", Lists.of("Basic dXNlcm5hbWU6cGFzc3dvcmQK"));
        assertThat(sink.getHeaders()).containsEntry("RequestId", Lists.of("request:12"));
        assertThat(sink.getHeaders()).containsEntry("MultiHeader", Lists.of("Override-me", "And-me"));
    }

    @Test
    void test_withDefaultHeaders_overridable() {
        // ARRANGE
        Map<String, List<String>> defaultHeaders = new LinkedHashMap<>();
        defaultHeaders.put("Authorization", Lists.of("Basic dXNlcm5hbWU6cGFzc3dvcmQK"));
        defaultHeaders.put("RequestId", Lists.of("request:12"));
        defaultHeaders.put("MultiHeader", Lists.of("Override-me", "And-me"));
        TestRequester requester = requester().withDefaultHeaders(defaultHeaders);

        // ACT
        requester.get("/echo")
                .withHeader("MultiHeader", "New value", "New second value", "New third value")
                .withHeader("Authorization", "Garbage")
                .execute()
                .assertStatusOk();

        // ASSERT
        assertThat(sink.getHeaders())
                .containsEntry("Authorization", Lists.of("Garbage"));
        assertThat(sink.getHeaders())
                .containsEntry("RequestId", Lists.of("request:12"));
        assertThat(sink.getHeaders())
                .containsEntry("MultiHeader", Lists.of("New value", "New second value", "New third value"));
    }

    @Test
    void test_withUriVariable() {
        // ACT
        requester().get("/echo/{id}?key={query}")
                .withQueryParam("another", "{please}")
                .withUriVariable("id", 100)
                .withUriVariable("query", "{unescaped}")
                .execute()
                .assertStatusOk();

        // ASSERT
        assertThat(sink.getUri()).isEqualTo("/echo/100");
        assertThat(sink.getQueryParams()).as("md.ts14ic.test.requester params")
                .containsEntry("key", Lists.of("{unescaped}"))
                .containsEntry("another", Lists.of("{please}"));
    }

    @Test
    void test_withQueryParams() {
        // ACT
        requester().get("/echo")
                .withQueryParam("key", "{url encode me}")
                .withQueryParam("numbers", "one", "two", "three")
                .execute()
                .assertStatusOk();

        // ASSERT
        assertThat(sink.getQueryParams())
                .containsEntry("key", Lists.of("{url encode me}"))
                .containsEntry("numbers", Lists.of("one", "two", "three"));
    }

    @Test
    void test_withRequestParams() {
        // ACT
        requester().get("/echo")
                .withRequestParam("key", "{url encode me}")
                .withRequestParam("numbers", "one", "two", "three")
                .execute()
                .assertStatusOk();

        // ASSERT
        assertThat(sink.getQueryParams())
                .containsEntry("key", Lists.of("{url encode me}"))
                .containsEntry("numbers", Lists.of("one", "two", "three"));
    }

    @Test
    void test_formUrlEncoded() {
        // ARRANGE
        // ACT
        Dto dto = requester().post("/echo-form")
                .withRequestParam("number", "20")
                .withRequestParam("text", "twenty")
                .withFormBody()
                .execute()
                .assertStatusOk()
                .getJsonBody(Dto.class);

        // ASSERT
        assertThat(dto).as("response body").isEqualTo(new Dto(20, "twenty"));
        assertThat(sink.getQueryParams()).containsEntry("number", Lists.of("20"));
        assertThat(sink.getQueryParams()).containsEntry("text", Lists.of("twenty"));
    }

    @Test
    void test_bytesBody() {
        // ACT
        byte[] body = Base64.getDecoder().decode("deadbeef");

        Dto dto = requester().post("/echo-base64").withBody(body).execute()
                .assertStatusOk()
                .getJsonBody(Dto.class);

        // ASSERT
        assertThat(dto).as("dto").isEqualTo(new Dto(6/*bytes*/, "deadbeef"));
    }

    @Test
    void test_stringBody() {
        // ACT
        Dto dto = requester().post("/echo")
                .withBody("{\"number\":12,\"text\":\"twelve\"}")
                .withHeader("Content-Type", "application/json")
                .execute()
                .assertStatusOk()
                .getJsonBody(Dto.class);

        // ASSERT
        assertThat(dto).as("dto").isEqualTo(new Dto(12, "twelve"));
    }

    @Test
    void test_getJsonBody() {
        // ARRANGE
        Dto expectedDto = new Dto(3, "three");

        // ACT
        Dto responseBody = requester().get("/echo").withJsonBody(expectedDto).execute()
                .assertStatusOk()
                .getJsonBody(Dto.class);

        // ASSERT
        assertThat(responseBody).as("Response body").isEqualTo(expectedDto);
    }

    @Test
    void test_getJsonBody_failure() {
        // ACT
        Throwable thrown = catchThrowable(() ->
                requester().get("/echo-string").withBody("not-json").execute()
                .assertStatusOk()
                .getJsonBody(Dto.class)
        );

        // ASSERT
        assertThat(thrown)
                .isInstanceOf(AssertionError.class)
                .hasMessageContaining("Expected body of type Dto but couldn't parse");
    }

    @Test
    void test_getGenericJsonBody() {
        // ARRANGE
        Dto expectedDto = new Dto(3, "three");

        // ACT
        List<Dto> responseBody = requester().get("/echo-twice").withJsonBody(expectedDto).execute()
                .assertStatusOk()
                .getJsonBody(new GenericType<List<Dto>>() {
                });

        // ASSERT
        assertThat(responseBody).as("Response body").isEqualTo(Arrays.asList(expectedDto, expectedDto));
    }

    @Test
    void test_getGenericJsonBody_failure() {
        // ACT
        Throwable thrown = catchThrowable(() ->
                requester().get("/echo-string").withBody("not-json").execute()
                        .assertStatusOk()
                        .getJsonBody(new GenericType<List<Dto>>() {})
        );

        // ASSERT
        assertThat(thrown)
                .isInstanceOf(AssertionError.class)
                .hasMessageContaining("Expected body of type java.util.List<com.gitlab.testrequester.Dto> but couldn't parse");
    }

    @Test
    void test_everythingAtOnce() {
        // ACT
        Dto dto = new Dto(25, "twenty-five");
        TestResponse response = requester().post("/echo/{id}")
                .withUriVariable("id", "4567")
                .withQueryParam("foo", "bar", "{escape-me}")
                .withJsonBody(dto)
                .withHeader("CustomHeader", "some-value")
                .execute()
                .assertStatusOk();

        // ASSERT
        assertThat(response.getJsonBody(Dto.class)).as("Response mapped")
                .isEqualTo(dto);

        assertThat(sink.getMethod()).as("HTTP method")
                .isEqualTo("POST");
        assertThat(sink.getUri()).as("Path variable id")
                .isEqualTo("/echo/4567");
        assertThat(sink.getBody()).as("RequestBody")
                .isEqualTo(dto);
        assertThat(sink.getHeaders()).as("Headers")
                .containsEntry("Content-Type", Lists.of("application/json"));
        assertThat(sink.getQueryParams()).as("Query params")
                .containsEntry("foo", Lists.of("bar", "{escape-me}"));
    }

    @Test
    void test_assertStatus() {
        // ARRANGE
        @Value
        class Param {
            int status;
            Consumer<TestResponse> assertMethod;
        }
        SoftAssertions.assertSoftly(softly -> Arrays.asList(
                new Param(200, TestResponse::assertStatusOk),
                new Param(201, TestResponse::assertStatusCreated),
                new Param(204, TestResponse::assertStatusNoContent),
                new Param(200, TestResponse::assertStatus2xx),
                new Param(201, TestResponse::assertStatus2xx),
                new Param(204, TestResponse::assertStatus2xx),

                new Param(400, TestResponse::assertStatusBadRequest),
                new Param(401, TestResponse::assertStatusUnauthorized),
                new Param(403, TestResponse::assertStatusForbidden),
                new Param(404, TestResponse::assertStatusNotFound),
                new Param(400, TestResponse::assertStatus4xx),
                new Param(401, TestResponse::assertStatus4xx),
                new Param(403, TestResponse::assertStatus4xx),
                new Param(404, TestResponse::assertStatus4xx),

                new Param(500, TestResponse::assertStatusInternalServerError),
                new Param(500, TestResponse::assertStatus5xx)
        ).forEach(param -> {
            // ACT
            TestResponse response = requester().get("/echo-status?status=" + param.status).execute();

            // ASSERT
            softly.assertThatCode(() -> param.assertMethod.accept(response))
                    .doesNotThrowAnyException();
        }));
    }

    @Test
    void test_getHeaders() {
        // ACT
        TestResponse response = requester().get("/echo-headers")
                .withHeader("Foo", "1", "one")
                .withHeader("Bar", "2")
                .execute()
                .assertStatusOk();

        // ASSERT
        assertThat(response.getHeaderFirst("Foo")).as("Header 'Foo' first").contains("1");
        assertThat(response.getHeaders("Foo")).as("Header 'Foo'").isEqualTo(Lists.of("1", "one"));
        assertThat(response.getHeaders("Bar")).as("Header 'Bar'").isEqualTo(Lists.of("2"));
    }

    @Test
    void test_assertStatus_failure() {
        // ARRANGE
        @Value
        class Param {
            int status;
            Consumer<TestResponse> assertMethod;
            String message;
        }
        SoftAssertions.assertSoftly(softly -> Arrays.asList(
                new Param(200, response -> response.assertStatus(300), "expected: <300> but was: <200>"),
                new Param(200, TestResponse::assertStatus4xx, "expected: <4XX> but was: <200>"),
                new Param(200, TestResponse::assertStatus5xx, "expected: <5XX> but was: <200>"),
                new Param(200, TestResponse::assertStatusCreated, "expected: <201> but was: <200>"),
                new Param(404, TestResponse::assertStatus2xx, "expected: <2XX> but was: <404>")
        ).forEach(param -> {
            // ACT
            TestResponse response = requester().get("/echo-status?status=" + param.status).execute();

            // ASSERT
            softly.assertThatCode(() -> param.assertMethod.accept(response))
                    .isInstanceOf(AssertionError.class)
                    .hasMessageContaining(param.message);
        }));
    }
}